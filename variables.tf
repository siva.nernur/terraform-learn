variable "region" {
  description = "GCP region"
  default = "asia-south1"
}

variable "machine_type" {
  description = "Type of GCP VM to provision"
  default = "e2-standard-4"
}

variable "instance_name" {
  description = "GCP vm name"
  default = "instance-from-terraform"
}
