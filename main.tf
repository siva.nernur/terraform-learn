terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.78.0"
    }
  }

  backend "remote" {
    organization = "dnsforpolicy"

    workspaces {
      name = "sentinel-example"
    }
  }
}

provider "google" {
  # Configuration options
  credentials = "${file("service-account.json")}"
  project     = "linear-trees-320904"
  region      = "asia-south1"
  zone        = "asia-south1-c"
}

resource "google_compute_instance" "default" {
  name         = "instance-from-terraform"
  machine_type = "e2-standard-4"

  tags = ["foo", "bar"]

  boot_disk {
    initialize_params {
      image = "debian-10-buster-v20210721"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata = {
    foo = "bar"
  }

  metadata_startup_script = "echo hi > /test.txt"
}
